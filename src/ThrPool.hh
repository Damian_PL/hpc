#ifndef THR_POOL_HH
#define THR_POOL_HH

#include <vector>
#include <pthread.h>

#include "Grafo.hh"

class ThrPool {
    private:
        std::vector<pthread_t> _threads;
        int _size;
        bool _started;
        pthread_cond_t* _c_started;
        pthread_mutex_t* _c_m_started;

        pthread_mutex_t* _reg_res;
        std::vector<int> _comb;
        double _perdida_tot;

        std::vector< std::vector<int> > _combs;
        const Grafo* _g;
        int _cant_combs;
    public:
        ThrPool(int n, const Grafo* g, int cant_combs);
        ~ThrPool();

        void start(std::vector< std::vector<int> > combs);
        std::vector< std::vector<int> > combs() const;

        bool started();
        pthread_cond_t* c_started();
        pthread_mutex_t* c_m_started();

        void register_result(std::vector<int> comb, double perdida);
        void synchronize();

        double perdida_tot();
        std::vector<int> comb();
};

#endif
