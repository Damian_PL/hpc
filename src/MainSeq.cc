#include <iostream>
#include <vector>
#include <limits>
#include <fstream>
#include <string>
#include <cstdlib>

#include "Grafo.hh"
#include "Helpers.hh"

int main(int argc, char** argv) {
    if (argc < 2){
        std::cout << "Error. Uso (p.ej): ./Seq ./mAdy.txt 6" << std::endl;
        exit(1);
    }

    std::ifstream fs(argv[1]);
    if (!fs) {
        std::cout << "No se pudo leer el archivo " << argv[1] << std::endl;
        exit(1);
    }

    Grafo* g_ = new Grafo();
    fs >> *g_;

    const Grafo* g = new Grafo(*g_);
    delete g_;

    int k = atoi(argv[2]);

    try{
        std::vector< std::vector<int> > combs = helpers::combinaciones(g->nodos(), k);

        double min_perdida = std::numeric_limits<double>::infinity();
        std::vector<int> comb_opt = std::vector<int>(k);

        for(int i = 0; i < combs.size(); i++) {
            Grafo* g_ = g->extraer_nodos(combs[i]);
            std::vector<Grafo*> componentes = g_->componentes_conexas();

            double perdida = 0.0;
            for(int j = 0; j < componentes.size(); j++)
                perdida += ((double)componentes[j]->nodos() / (double)g_->nodos()) * componentes[j]->calcular_perdida();

            if (perdida < min_perdida) {
                min_perdida = perdida;
                comb_opt = combs[i];
            }

            delete g_;
        }

        std::cout << "Mejor combinacion: ";
        for (int i = 0; i < comb_opt.size(); i++)
            std::cout << comb_opt[i] << " ";
        std::cout << std::endl;

        std::cout << "Perdida esperada: " << min_perdida << std::endl;

    } catch (std::string msg) {
        std::cout << msg << std::endl;
        exit(1);
    }

    delete g;
    return(0);
}
