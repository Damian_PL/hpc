#include "Helpers.hh"
#include <algorithm>

std::vector< std::vector<int> > helpers::combinaciones(int m, int n) {
    std::vector<bool> v(m);
    std::fill(v.begin(), v.begin() + n, true);
    std::vector< std::vector<int> > combs;
    std::vector<int> sample;

    do {
        for (int i = 0; i < m; ++i) {
            if (v[i]) sample.push_back(i + 1);
        }
        combs.push_back(sample);
        sample.clear();
    } while (std::prev_permutation(v.begin(), v.end()));

    return combs;
}
