#include "Grafo.hh"
#include <cstdio>
#include <algorithm>

Grafo::Grafo() {
    _matAdyacencia = std::vector< std::vector<bool> >();
}

Grafo::Grafo(unsigned int n) {
    _matAdyacencia = std::vector< std::vector<bool> >(n);
    for (int i=0; i<n; i++){
        _matAdyacencia[i] = std::vector<bool>(n);
        for (int j=0; j<n; j++)
            _matAdyacencia[i][j] = false;
    }
}

Grafo::Grafo(const Grafo& g) {
    int tam = g._matAdyacencia.size();
    _matAdyacencia = std::vector< std::vector<bool> >(tam);
    for(int i = 0; i < g._matAdyacencia.size(); i++) {
        _matAdyacencia[i] = std::vector<bool>(tam);
        for(int j = 0; j < _matAdyacencia[i].size(); j++)
            _matAdyacencia[i][j] = g._matAdyacencia[i][j];
    }
}

Grafo::Grafo(const std::vector< std::vector<bool> >& matAdyacencia) {
    int cant_vertices = matAdyacencia.size();
    _matAdyacencia = std::vector< std::vector<bool> >(cant_vertices);
    for (int i=0; i<cant_vertices; i++){
        _matAdyacencia[i] = std::vector<bool>(cant_vertices);
        for (int j=0; j<cant_vertices; j++)
            _matAdyacencia[i][j] = matAdyacencia[i][j];
    }
}

Grafo::~Grafo() {
    for(int i=0; i<_matAdyacencia.size(); i++)
        _matAdyacencia[i].clear();

    _matAdyacencia.clear();
}

std::istream& operator>> (std::istream &is, Grafo &g){
    std::string line;
    int i = 0;
    int n;

    bool read = static_cast<bool>(getline(is, line));
    n = (line.size() + 1) / 2;

    g._matAdyacencia = std::vector< std::vector<bool> > (n);

    while(i < n){
        g._matAdyacencia[i] = std::vector<bool>(n);

        for(int j = 0; j < n; j++)
            g._matAdyacencia[i][j] = (line[j*2] == '1');

        read = static_cast<bool>(getline(is, line));
        ++i;
    }

    return is;
}

std::ostream& operator<< (std::ostream& os, Grafo& g) {
    for (int i = 0; i < g._matAdyacencia.size(); i++) {
        for(int j = 0; j < g._matAdyacencia[i].size(); j++)
            os << g._matAdyacencia[i][j] << " ";

        os << std::endl;
    }

    return os;
}

int Grafo::nodos() const {
    return _matAdyacencia.size();
}

bool Grafo::valor_matriz(int i, int j) const {
    return _matAdyacencia[i][j];
}

std::vector< Grafo* > Grafo::componentes_conexas() const {
    //std::vector< Grafo* > ret = std::vector<Grafo*>();
    //ret.push_back(new Grafo(*this));
    //return ret;
    std::vector< Grafo* > total_componentes;
    std::vector< std::vector<bool> > matAccesibilidad = matriz_accesibilidad();
    std::set<int> componente;
    std::vector<bool> vertices;
    int cant_vertices = nodos();

    for (int i = 0; i < cant_vertices; ++i) {
        vertices.push_back(true);
    }

    for (int i = 0; i < cant_vertices; ++i) {
        if (vertices[i]) {
            std::vector<bool> fila = matAccesibilidad[i];

            for (int j = 0; j < cant_vertices; ++j) {
                if (fila[j] && matAccesibilidad[j][i]) {
                    componente.insert(j);
                    vertices[j] = false;
                }
            }

            std::vector< std::vector<bool> > matriz = generar_matriz(componente);
            Grafo* subgrafo = new Grafo(matriz);
            total_componentes.push_back(subgrafo);
            componente.clear();
        }
    }

    return total_componentes;
}

std::vector< std::vector<bool> > Grafo::generar_matriz(std::set<int> vertices) const {
    int cant_vertices = vertices.size();
    int total = nodos();
    int fila = 0;
    int col = 0;

    std::vector< std::vector<bool> > matAdyacencia = std::vector< std::vector<bool> >(cant_vertices);
    for (int i = 0; i < total; ++i) {
        if (vertices.find(i) != vertices.end()) {
            matAdyacencia[fila] = std::vector<bool>(cant_vertices);
            for (int j = 0; j < total; ++j) {
                if (vertices.find(j) != vertices.end()) {
                    matAdyacencia[fila][col] = valor_matriz(i, j);
                    col++;
                }
            }
            fila++;
            col = 0;
        }
    }

    return matAdyacencia;
}

void Grafo::recorrer_fila(std::set<int>& alcanzables, int fila) const {
    int cant_vertices = nodos();
    for (int j = 0; j < cant_vertices; ++j) {
        bool no_pertenece = alcanzables.find(j) == alcanzables.end();
        if (valor_matriz(fila,j) && no_pertenece) {
            alcanzables.insert(j);
            recorrer_fila(alcanzables, j);
        }
    }
}

double Grafo::calcular_perdida() const {
    double perdida = 0.0;
    for (int i = 0; i < _matAdyacencia.size(); i++) {
        std::vector<bool> aristasNodo = _matAdyacencia[i];
        for (int j = 0; j < aristasNodo.size(); j++) {
            if (j) perdida++;
        }
    }
    return perdida;
}

std::vector< std::vector<bool> > Grafo::matriz_accesibilidad() const {
    int cant_vertices = nodos();
    std::vector< std::vector<bool> > matAccesibilidad = std::vector< std::vector<bool> >(cant_vertices);
    std::set<int> alcanzables;
    bool in_set;

    for (int i=0; i<cant_vertices; i++){
        matAccesibilidad[i] = std::vector<bool>(cant_vertices);
        alcanzables.insert(i);

        recorrer_fila(alcanzables, i);

        for (int j=0; j<cant_vertices; j++) {
            in_set = alcanzables.find(j) != alcanzables.end();
            matAccesibilidad[i][j] = in_set;
        }

        alcanzables.clear();

    }

    return matAccesibilidad;
}

Grafo* Grafo::extraer_nodos(std::vector<int> comb) const {
    std::vector< std::vector<bool> > mat_ady = std::vector< std::vector<bool> >();
    for(int i = 0; i< _matAdyacencia.size(); i++) {
        if (std::find(comb.begin(), comb.end(), i) != comb.end()) continue;

        std::vector<bool> n_row = std::vector<bool>();
        for(int j = 0; j < _matAdyacencia[i].size(); j++) {
            if (std::find(comb.begin(), comb.end(), j) != comb.end()) continue;
            n_row.push_back(_matAdyacencia[i][j]);
        }
        mat_ady.push_back(n_row);
    }

    return new Grafo(mat_ady);
}
