#include <iostream>
#include <fstream>
#include <cstdlib>
#include <pthread.h>
#include <limits>
#include <string>

#include "Grafo.hh"
#include "Helpers.hh"
#include "ThrPool.hh"

int main(int argc, char** argv) {
    /*
        Args:
            1 - path del archivo con matriz de adyacencia
            2 - k (cantidad nodos a inmunizar)
            3 - cant threads
    */

    if (argc < 3){
        std::cout << "Error. Uso (p.ej): ./Paral ./mAdy.txt 6 2" << std::endl;
        exit(1);
    }

    std::ifstream fs(argv[1]);
    if (!fs) {
        std::cout << "No se pudo leer el archivo " << argv[1] << std::endl;
        exit(1);
    }

    Grafo* g_ = new Grafo();
    fs >> *g_;

    const Grafo* g = new Grafo(*g_);
    delete g_;

    int k = atoi(argv[2]);
    int t_size = atoi(argv[3]);
    int cant_combs = helpers::combinaciones(g->nodos(), k).size();
    ThrPool* thrPool = new ThrPool(t_size, g, cant_combs);

    try {
        std::vector< std::vector<int> > combs = helpers::combinaciones(g->nodos(), k);
        thrPool->start(combs);
        thrPool->synchronize();

        std::vector<int> comb = thrPool->comb();
        double perdida = thrPool->perdida_tot();

        if(comb.size() == 0) {
            std::cout << "Error de procesamiento. Combinacion encontrada invalida" << std::endl;
            exit(1);
        }

        std::cout << "Mejor combinacion: ";
        for (int i = 0; i < comb.size(); i++)
            std::cout << comb[i] << " ";
        std::cout << std::endl;

        std::cout << "Perdida esperada: " << perdida << std::endl;
    } catch (std::string msg) {
        std::cout << msg << std::endl;
        exit(1);
    }

    return(0);
}
