#ifndef GRAFO_HH
#define GRAFO_HH

#include <vector>
#include <set>
#include <iostream>
#include <string>

class Grafo {
    private:
        std::vector< std::vector<bool> > _matAdyacencia;

        std::vector< std::vector<bool> > generar_matriz(std::set<int> vertices) const;
        void recorrer_fila(std::set<int>& alcanzables, int fila) const;
        std::vector< std::vector<bool> > matriz_accesibilidad() const;
    public:
        Grafo();
        Grafo(unsigned int n);
        Grafo(const Grafo& g);
        Grafo(const std::vector< std::vector<bool> >& matAdyacencia);
        ~Grafo();

        friend std::istream& operator>> (std::istream& is, Grafo& g);
        friend std::ostream& operator<< (std::ostream& os, Grafo& g);

        int nodos() const;
        bool valor_matriz(int i , int j) const;
        std::vector< Grafo* > componentes_conexas() const;
        double calcular_perdida() const;
        Grafo* extraer_nodos(std::vector<int> comb) const;
};

#endif
