#ifndef HELPERS_HH
#define HELPERS_HH

#include <vector>

namespace helpers {
    std::vector< std::vector<int> > combinaciones(int m, int n);
}

#endif
