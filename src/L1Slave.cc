/*
    hwloc ref:
    https://github.com/open-mpi/hwloc/blob/master/doc/examples/hwloc-hello.c
*/

#include <vector>
#include <pthread.h>
#include <limits>
#include "L1Slave.hh"
#include "ThrPool.hh"
#include "Grafo.hh"

#include <unistd.h>

void* L1Slave::run(void* args) {
    L1Slave::args* args_ = (L1Slave::args*) args;

    int cant_cpus = sysconf(_SC_NPROCESSORS_ONLN);
    cpu_set_t cpus;
    CPU_ZERO(&cpus);
    CPU_SET((args_->id % cant_cpus), &cpus);

    pthread_t id = pthread_self();
    pthread_setaffinity_np(id, sizeof(cpu_set_t), &cpus);

    ThrPool* pool = args_->pool;

    pthread_mutex_t* m = pool->c_m_started();
    pthread_cond_t* cond = pool->c_started();

    pthread_mutex_lock(m);
    while(!pool->started())
        pthread_cond_wait(cond, m);
    pthread_mutex_unlock(m);

    // Calcular rango de trabajo con los datos ingreados en args
    int a = args_->id * args_->combs_p_thread;
    int b = a + args_->combs_p_thread - 1;

    // Combs por thread no es multiplo de tot combs y es ultimo thread
    if (args_->id == args_->cant_thr - 1 && (args_->cant_combs % args_->combs_p_thread) != 0)
        b = args_->cant_combs - 1;

    std::vector< std::vector<int> > combs = pool->combs();

    double min_perdida = std::numeric_limits<double>::infinity();
    std::vector<int> comb_opt = std::vector<int>();

    for (int i = a; i <= b; i++) {
        Grafo* g_ = args_->g->extraer_nodos(combs[i]);
        if (g_->nodos() == 0) continue;

        std::vector<Grafo*> componentes = g_->componentes_conexas();
        double perdida = 0.0;
        for (int j = 0; j < componentes.size(); j++)
            perdida += ((double)componentes[j]->nodos() / (double)g_->nodos()) * componentes[j]->calcular_perdida();

        if (perdida < min_perdida) {
            min_perdida = perdida;
            comb_opt.clear();
            comb_opt = combs[i];
        }
    }
    pool->register_result(comb_opt, min_perdida);
    return NULL;
}
