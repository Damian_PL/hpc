#include "ThrPool.hh"
#include "L1Slave.hh"

#include <cmath>
#include <limits>

ThrPool::ThrPool(int n, const Grafo* g, int cant_combs): _size(n), _g(g), _cant_combs(cant_combs) {
    _threads = std::vector<pthread_t>(n);
    _started = false;
    _comb = std::vector<int>();
    _perdida_tot = std::numeric_limits<double>::infinity();
    _combs = std::vector< std::vector<int> >();

    _c_started = new pthread_cond_t;
    _c_m_started = new pthread_mutex_t;
    _reg_res = new pthread_mutex_t;

    pthread_cond_init(_c_started, NULL);
    pthread_mutex_init(_c_m_started, NULL);
    pthread_mutex_init(_reg_res, NULL);

    for(int i = 0; i < n; i++) {
        pthread_t id;
        _threads[i] = id;

        L1Slave::args* slave_args = new L1Slave::args();
        slave_args->pool = this;
        slave_args->id = i;
        slave_args->cant_thr = n;
        slave_args->g = g;
        slave_args->cant_combs = cant_combs;
        slave_args->combs_p_thread = ceil((double)cant_combs / (double)n);

        int status = pthread_create(&_threads[i], NULL, L1Slave::run, (void*)slave_args);
        if(status != 0)
            std::cout << "Error creando thread: " << status << std::endl;
    }
}

ThrPool::~ThrPool() {
}

void ThrPool::start(std::vector< std::vector<int> > combs) {
    _combs = combs;
    _started = true;
    pthread_cond_broadcast(_c_started);
}

std::vector< std::vector<int> > ThrPool::combs() const {
    return _combs;
}

pthread_cond_t* ThrPool::c_started() {
    return _c_started;
}

pthread_mutex_t* ThrPool::c_m_started() {
    return _c_m_started;
}

bool ThrPool::started() {
    return _started;
}

void ThrPool::register_result(std::vector<int> comb, double perdida) {
    pthread_mutex_lock(_reg_res);

    if (perdida < _perdida_tot) {
        _perdida_tot = perdida;
        _comb.clear();
        _comb = comb;
    }
    pthread_mutex_unlock(_reg_res);
}

void ThrPool::synchronize() {
    for (int i = 0; i < _threads.size(); i++) {
        pthread_join(_threads[i], NULL);
    }
}

double ThrPool::perdida_tot() {
    return _perdida_tot;
}

std::vector<int> ThrPool::comb() {
    return _comb;
}
