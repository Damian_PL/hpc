#ifndef L1_SLAVE_HH
#define L1_SLAVE_HH

#include "Grafo.hh"
#include "ThrPool.hh"

namespace L1Slave {

struct args {
    ThrPool* pool;
    int id;
    int cant_thr;
    const Grafo* g;
    int cant_combs;
    int combs_p_thread;
};

void* run(void* args);

};

#endif
