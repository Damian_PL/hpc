FLAGS = -std=c++11 -Wall -Werror
CC = g++ $(FLAGS) -c
BIN = g++ $(FLAGS) -lpthread

WD = $(CURDIR)

SRC_PATH = $(WD)/src
BIN_PATH = $(WD)/bin

default:
	(make Seq)
	(make Paral)

# Executions

runp:
	$(WD)/bin/Paral $(WD)/data/Marpa.txt 6 8

runs:
	$(WD)/bin/Seq $(WD)/data/Marpa.txt 6

# Binaries

Seq: $(SRC_PATH)/Helpers.o $(SRC_PATH)/Grafo.o $(SRC_PATH)/MainSeq.o
	$(BIN) $^ -o $(BIN_PATH)/$@

Paral: $(SRC_PATH)/Helpers.o $(SRC_PATH)/Grafo.o $(SRC_PATH)/L1Slave.o $(SRC_PATH)/ThrPool.o $(SRC_PATH)/MainParallel.o
	$(BIN) $^ -o $(BIN_PATH)/$@

# Source files

MainSeq.o: $(SRC_PATH)/MainSeq.cc $(SRC_PATH)/Grafo.o $(SRC_PATH)/Helpers.o
	$(CC) $@

MainParallel.o: $(SRC_PATH)/MainParallel.cc $(SRC_PATH)/Grafo.o $(SRC_PATH)/Helpers.o $(SRC_PATH)/L1Slave.o $(SRC_PATH)/ThrPool.o
	$(CC) $@

L1Slave.o: $(SRC_PATH)/L1Slave.cc $(SRC_PATH)/L1Slave.hh $(SRC_PATH)/Grafo.o $(SRC_PATH)/ThrPool.o $(SRC_PATH)/Helpers.o
	$(CC) $@

Helpers.o: $(SRC_PATH)/Helpers.cc $(SRC_PATH)/Helpers.hh $(SRC_PATH)/ThrPool.o
	$(CC) $@

ThrPool.o: $(SRC_PATH)/ThrPool.cc $(SRC_PATH)/ThrPool.hh
	$(CC) $@

Grafo.o: $(SRC_PATH)/Grafo.cc $(SRC_PATH)/Grafo.hh
	$(CC) $@

# Miscellaneous

clean:
	(cd $(BIN_PATH); rm -f Paral)
	(cd $(BIN_PATH); rm -f Seq)
	(cd $(SRC_PATH); rm -f *.o *% *~)

